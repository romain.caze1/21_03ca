Pyhon script for the article in F1000 in 2021 by Romain Cazé

# Requirements
Pyhon 3+, Numpy, Matpltolib and Brian 2

# Running

   pyhon simulate.pyhon

This will generate the figure from the article
